locals {
  common_tags = {
    Environment = var.tag_environment
    Application = var.tag_application
    Owner       = var.tag_owner
    Criticality = var.tag_criticality
    ManagedBy   = var.tag_managedby
  }
}

variable "tag_application" {
  type = string
}

variable "tag_criticality" {
  type = string
}

variable "tag_environment" {
  type = string
}

variable "tag_owner" {
  type = string
}

variable "tag_managedby" {
  type = string
}

variable "zone_name" {
  type = string
}
