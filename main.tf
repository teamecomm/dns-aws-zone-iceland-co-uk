terraform {
  backend "remote" {
    organization = "iceland-foods"

    workspaces {
      name = "dns-aws-zone-iceland-co-uk"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}
