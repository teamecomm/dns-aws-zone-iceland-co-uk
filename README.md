# Iceland.co.uk - AWS & DNS Configuration #

Iceland.co.uk DNS is managed on AWS using Terraform. Previously the non-standard ALIAS type record was set on the APEX domain, which is not supported by trusted vendors such as AWS.

As a result, we have developed a Lambda@Edge function which forces a 301 from any non-www address to a www version, as well as enforcing HTTPS. 

The APEX record of the domain now points to a CloudFront distribution which triggers the Lambda@Edge function. The origin of this CloudFront distribution is an empty S3 bucket, which requires no contents or further configuration as it's only used as a dummy origin endpoint.

All configuration for CloudFront is on us-east-1.

### When to store DNS records in Terraform? ###

Only generic or standard records should be stored within the Terraform configuration. Any application or project specific DNS records should be included in their respective repistories.

For example, Office365 configurations, mail configuration, and any permanent records should be stored within Terraform.

###  What does the CloudFront configuration do? ###

* Creation of Cloudfront Distribution Endpoint
* Assignment of Lambda@Edge Function to CloudFront
* Assignment of Certificate from ACM for Iceland.ie

### What does the Route 53 configuration do? ###

* Creation of a Route 53 Hosted Zone
* Creation of A record (ALIAS) to CloudFront Zone ID

### What does the S3 configuration do? ###

* Creation of an empty S3 bucket for CloudFront to use as origin
* Configure bucket for use as static web hosting

### How To Deploy ###

* Commits to master are auto-planned within Terraform Cloud
* Once plan is complete and passed all policies, apply within Terraform Cloud

### Owner ###

* Luke Barber
* luke.barber@iceland.co.uk