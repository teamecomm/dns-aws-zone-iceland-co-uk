# ALIAS record for the APEX domain
# --------------------------------
# This is used in conjunction with a Lambda@Edge function to re-write
# all requests from non-www to www

# ALIAS records have a 60 minute TTL that is non-configurable on Route53
resource "aws_route53_record" "a" {
 zone_id = aws_route53_zone.zone.zone_id
 name    = aws_route53_zone.zone.name
 type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.s3_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.s3_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}