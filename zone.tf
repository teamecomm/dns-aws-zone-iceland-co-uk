resource "aws_route53_zone" "zone" {
  name = var.zone_name
  tags = local.common_tags
}