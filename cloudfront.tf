# Empty bucket required for alias association
resource "aws_s3_bucket" "b" {
  bucket = "iceland-dns-couk-apex"
  acl    = "private"

  # Set the S3 to static website hosting
  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  # Apply tags to resource
  tags = local.common_tags
}

# Values for Lambda@Edge and ACM
locals {
  s3_origin_id = "Iceland.co.uk - Apex DNS"
  lambda_edge_function = "arn:aws:lambda:us-east-1:752255461418:function:R53_Apex_redirects:23"
  iceland_couk_r53cert = "arn:aws:acm:us-east-1:752255461418:certificate/f6c47c11-49da-4571-936a-66c89d6db84f"
}

# CloudFront configuration
resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = aws_s3_bucket.b.bucket_regional_domain_name
    origin_id   = local.s3_origin_id
  }

  # Additional domains for the distribution 
  # These must be included in the viewer certificate
    aliases = [
    "iceland.co.uk", 
    "www.iceland.co.uk"
  ]

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Iceland.co.uk - Apex DNS"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
  
    # Associate the lambda function to the viewer-request action
    lambda_function_association {
      event_type   = "viewer-request"
      lambda_arn   = local.lambda_edge_function
      include_body = false
    }
  }

  # Price class requires only US, Canada & EU given our audience
  price_class = "PriceClass_100"

  # No restrictions required but specification necessary
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  # Apply tags to resource
  tags = local.common_tags

  # Use SSL certificate located in ACM
  viewer_certificate {
    acm_certificate_arn = local.iceland_couk_r53cert
    ssl_support_method = "sni-only"
    minimum_protocol_version = "TLSv1.2_2018"
  }
  
}