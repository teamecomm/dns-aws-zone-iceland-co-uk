# Output the CloudFront domain name
output "CloudFront_Domain" {
  value = aws_cloudfront_distribution.s3_distribution.domain_name
}

output "S3_Bucket_Hostname" {
  value = aws_s3_bucket.b.website_domain
}